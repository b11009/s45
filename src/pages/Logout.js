import { useContext, useEffect } from 'react';
import { Navigate } from 'react-router-dom';
import UserContext from '../UserContext';

export default function Logout(){

	const { unsetUser, setUser} = useContext(UserContext);


	// localStorage.clear();
	// Clear the local storage of the users information
	// trigger unSet
	unsetUser();

	// Adding useEffect that will allow the logout page to render first
	useEffect(() => {
		// Set the user state back to its original value
		setUser({id: null});
	}) 

	// Redirect to Login Page
	return(
		<Navigate to="/login" />
	)
}